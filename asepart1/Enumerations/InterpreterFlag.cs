﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asepart1
{
    [FlagsAttribute]
    enum InterpreterFlag
    {
        MethodFlag = 1,
        IfFlag = 2,
        LoopFlag = 4
    }
}
