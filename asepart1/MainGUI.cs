﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace asepart1
{
    public partial class MainGUI : Form
    {
        #region Fields
        //Public fields within GUI. (Not publicly accessible.)
        private Bitmap myBitmap;
        private Graphics g;

        private Interpreter i;
        private DrawData dData = new DrawData(Color.Black, Color.Transparent, 1, new Point(0,0));
        #endregion

        public MainGUI()
        {
            InitializeComponent();
        }
        private void MainGUI_Load(object sender, EventArgs e)
        {
            GenerateGraphicsPanel();
            string[] code = new string[] {
                    "%var1 = 100",
                    "%var2 = 200",
                    "if %var1 == %var2",
                    "shape rectangle(%var1,%var2)",
                    "shape movepen(300,400)",
                    "shape rectangle(%var2,%var1)",
                    "ifend"
                };
            richTxtBoxCmdLine.Lines = code;

            i = new Interpreter(g);
        }

        #region GUIMethods
        /// <summary>
        /// Generates the Graphics Panel upon GUI Load
        /// </summary>
        private void GenerateGraphicsPanel()
        {
            Console.WriteLine("test");
            DoubleBuffered = true;

            myBitmap = new Bitmap(pictureBoxGDI.Size.Width, pictureBoxGDI.Size.Height);
            g = Graphics.FromImage(myBitmap); //can be accessed from within the form
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias; // stops pixel perfect vectors, blends to be soft, aka Anti Aliasing
            g.Clear(Color.White); //white background

            pictureBoxGDI.Image = myBitmap; // assigning image to bitmap
        }
        /// <summary>
        /// Loads the graphics from the file
        /// </summary>
        private void LoadGraphicsFromFile()
        {
            Image image;
            
            openFileDialog.ShowDialog();

            image = Image.FromFile(openFileDialog.FileName);

            if (image.Width > pictureBoxGDI.Width || image.Height > pictureBoxGDI.Height)
                throw new FileLoadException("Image Size is bigger than canvas");
            else
                g.DrawImage(image, 0, 0);
        }
        /// <summary>
        /// Saves the graphics to file
        /// </summary>
        private void SaveGraphicsToFile()
        {
            string saveToLoc;

            saveFileDialog.ShowDialog();

            saveToLoc = saveFileDialog.FileName;
            if(saveToLoc.Trim().Length != 0)
                myBitmap.Save(saveToLoc);
        }
        private void ClearGraphics(Graphics gdi)
        {
            gdi.Clear(Color.White);
            pictureBoxGDI.Invalidate();
        }
        private void RunScript(string[] lines)
        {
            try
            {
                i.RunScript(lines);
                pictureBoxGDI.Invalidate();
            }
            catch (UnknownCallException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            catch (MethodExistsInDictionaryException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            catch (FailiureToRunScriptException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            catch (MathNotReturnedToVariableException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }
        #endregion

        #region SavingLoadStateEventMethods
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you would like to start again?", "Save your art?", MessageBoxButtons.YesNo);
            if(result == DialogResult.Yes)
                ClearGraphics(g);
        }
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                LoadGraphicsFromFile();
            }
            catch(FileLoadException ex)
            {
                MessageBox.Show("Exception Caught: " + ex);
            }
            
        }
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveGraphicsToFile();
        }
        #endregion
        #region DrawPenSettings
        private void customizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CustomiseDialog customiseDialog = new CustomiseDialog();
            customiseDialog.ShowDialog();
            dData = new DrawData(
                customiseDialog.clr, 
                customiseDialog.fillClr, 
                customiseDialog.thickness, 
                new Point(i.DrawData.GetOriginPosition()[0], i.DrawData.GetOriginPosition()[1])
                );
            i.DrawData = dData;
        }
        #endregion
        #region ButtonMethods
        private void btnSingleLineExec_Click(object sender, EventArgs e)
        {
            RunScript(new string[] { txtBoxCmdLine.Text });
        }
        private void btnMultiLineExec_Click(object sender, EventArgs e)
        {
            RunScript(richTxtBoxCmdLine.Lines);
        }
        #endregion
    }
}
