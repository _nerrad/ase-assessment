﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace asepart1
{
    public partial class CustomiseDialog : Form
    {
        public Color clr = Color.Black;
        public Color fillClr = Color.Transparent;
        public float thickness = 1;
        
        
        public CustomiseDialog()
        {
            InitializeComponent();
        }

        private void CustomiseDialog_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            colorDialogFill.Color = fillClr;
            colorDialogFill.ShowDialog();
            fillClr = colorDialogFill.Color;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            colorDialogBorder.Color = clr;
            colorDialogBorder.ShowDialog();
            clr = colorDialogBorder.Color;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            thickness = trackBar1.Value;
        }
    }
}
