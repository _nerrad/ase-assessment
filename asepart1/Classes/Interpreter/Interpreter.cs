﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asepart1
{
    public class Interpreter
    {
        ShapeFactory factory = new ShapeFactory();
        DrawData drawData = new DrawData(Color.Black, Color.Transparent, 1, new Point(0, 0));
        Graphics g;

        public DrawData DrawData
        {
            get { return drawData; }
            set { drawData = value; }
        }

        #region Dictionaries
        private Dictionary<string, MethodData> _SavedMethods = new Dictionary<string, MethodData>();
        private Dictionary<string, VariableData> _SavedVariables = new Dictionary<string, VariableData>();
        private Dictionary<string, VariableData> _MethodArguments = new Dictionary<string, VariableData>();

        private Dictionary<string, int> existingShapes                  = new Dictionary<string, int>(); //SHAPENAME, ARGCOUNT
        private Dictionary<string, string> regex_expressions            = new Dictionary<string, string>();
        #endregion

        /// <summary>
        /// Constructor to interpret data parsed through
        /// </summary>
        /// <param name="data">DrawData (ie, pen, width, etc)</param>
        /// <param name="g">Graphics object</param>
        public Interpreter(Graphics g)
        {
            this.g = g;

            existingShapes.Add("CIRCLE", 1);
            existingShapes.Add("LINE", 2);
            existingShapes.Add("RECTANGLE", 2);
            existingShapes.Add("TRIANGLE", 2);

            //@ = removes escape issues
            regex_expressions.Add("funccall", @"^(?<methodname>[a-z|A-Z]+)\((?<args>(?:(?:,)*?(?:\w)*?(?:\%\w)*?)*)\)$");
            regex_expressions.Add("func", @"^(?<call>func)\s(?<methodname>[a-z|A-Z]*)\((?<args>(?:,*?\w+?)*)\)$");
            regex_expressions.Add("funcend", @"^(funcend)$"); //tried and tested
            regex_expressions.Add("loopcall", @"^(?<keyword>loop)\s(for)\s(?<val>\d+)$");
            regex_expressions.Add("loopend", @"^(loopend)$");
            regex_expressions.Add("if", @"^(?:(?<keyword>if)\s(?<condi1>\%\w+|[0-9]+)\s(?<operator>(?:==)|(?:!=))\s+?(?<condi2>\%\w+|[0-9]+))$"); //tried and tested
            regex_expressions.Add("ifend", @"^(ifend)$"); //tried and tested
            regex_expressions.Add("varinit", @"^(?<init>%)(?<varName>\w*)\s=\s(?<val>(?:\d+)|math\((?:\d+|%\w+)\s(?:[\+|\-|\*|\/])\s(?:\d+|%\w+)\))$"); 
            regex_expressions.Add("varcall", @"(?<init>%)(?<varName>\w+)"); //tried and tested 
            regex_expressions.Add("math", @"math\((?<val1>\d+|%\w+)\s(?<operand>[\+|\-|\*|\/])\s(?<val2>\d+|%\w+)\)"); //tried and tested
            regex_expressions.Add("shapecall", @"^shape\s(?<shapeName>[a-z|A-Z]+)\((?<args>(?:(?:,)*?(?:\w)*?(?:\%\w)*?)*)\)$");
            regex_expressions.Add("comment", @"^#.*$");
        }

        #region Internal Management of Tasks
        /// <summary>
        /// Does a simple if statement with imported values
        /// </summary>
        /// <param name="val1">integer digit only... should convert from string</param>
        /// <param name="op">either '==' or '!=' only available to use</param>
        /// <param name="val2"></param>
        /// <returns></returns>
        private bool DoCondition(int val1, string op, int val2)
        {
            bool ret = false;

            if(op == "==")
            {
                if (val1 == val2)
                    ret = true;
                else
                    ret = false;
            }
            else if(op == "!=")
            {
                if (val1 != val2)
                    ret = true;
                else
                    ret = false;
            }
            else
            {
                //THROW EXCEPTION
            }
            return ret;
        }
        /// <summary>
        /// Does a simpe mathematical equation to
        /// </summary>
        /// <param name="val1">value 1 - integer</param>
        /// <param name="op">Operator for equation</param>
        /// <param name="val2">value 2 - integer</param>
        /// <param name="result">returns the result into the function call</param>
        /// <returns>error check, false if cannot proceed with function</returns>
        private bool DoMath(int val1, string op, int val2, out int result)
        {
            bool error = false;
            result = 0;

            if (op == "+")
            {
                result = val1 + val2;
            }
            else if (op == "-")
            {
                result = val1 - val2;
            }
            else if (op == "/")
            {
                result = val1 / val2;
            }
            else if (op == "*")
            {
                result = val1 * val2;
            }
            else
            {
                error = true;
            }
            return error;
        }
        /// <summary>
        /// Creates or Sets an existing variable within dictionary
        /// </summary>
        /// <param name="varname">Name of Key for Dictionary</param>
        /// <param name="varval">Name of Key for </param>
        /// <returns>True if success, otherwise false.</returns>
        private bool SetVariable(string varname,string varval)
        {
            bool success = true;

            if (varname.Substring(0, 1) == "%")
                varname = varname.Remove(0, 1);

            if (!_SavedVariables.ContainsKey(varname))
            {
                VariableData var = new VariableData()
                {
                    varData = varval,
                    varName = varname
                };
                _SavedVariables.Add(varname,var);
            }
            else
            {
                VariableData existingVar;
                success = _SavedVariables.TryGetValue(varname, out existingVar);

                if (!success)
                    return success;

                existingVar.varData = varval;

                _SavedVariables.Remove(varname);
                _SavedVariables.Add(varname, existingVar);
            }
            return success;
        }
        /// <summary>
        /// retrieve variable value from key
        /// </summary>
        /// <param name="varname">also key, stored in dictonary</param>
        /// <returns>VariableData - able to fully utilise this</returns>
        private int GetVariableData(string varname)
        {
            if (varname.Substring(0, 1) == "%")
                varname = varname.Remove(0,1);
            VariableData variableData;
            bool success = _SavedVariables.TryGetValue(varname, out variableData);
            return Convert.ToInt16(variableData.varData);
        }
        #endregion
        /// <summary>
        /// Checks syntax for the line inputted against regex expressions
        /// </summary>
        /// <param name="line">single line of the script</param>
        /// <returns>returns the TKey in string form.</returns>
        private string Regex_ConfirmSyntax(string line)
        {
            string confirm = null;
            char nonRegexSyntax = '^';
            for (int i = 0; i < regex_expressions.Count; i++)
            {
                if (Regex.IsMatch(line, regex_expressions["func"]))
                {
                    confirm = "func";
                    break;
                }
                if (Regex.IsMatch(line, regex_expressions["funcend"]))
                {
                    confirm = "funcend";
                    break;
                }
                if (Regex.IsMatch(line, regex_expressions["funccall"]))
                {
                    confirm = "funccall";
                    break;
                }
                if (Regex.IsMatch(line, regex_expressions["loopcall"]))
                {
                    confirm = "loopcall";
                    break;
                }
                if (Regex.IsMatch(line, regex_expressions["loopend"]))
                {
                    confirm = "loopend";
                    break;
                }
                if (Regex.IsMatch(line, regex_expressions["shapecall"]))
                {
                    confirm = "shapecall";
                    break;
                }
                if (Regex.IsMatch(line, regex_expressions["if"]))
                {
                    confirm = "if";
                    break;
                }
                if (Regex.IsMatch(line, regex_expressions["ifend"]))
                {
                    confirm = "ifend";
                    break;
                }
                if (Regex.IsMatch(line, regex_expressions["varinit"]))
                {
                    confirm = "varinit";
                    break;
                }
                if (Regex.IsMatch(line, regex_expressions["varcall"]))
                {
                    confirm = "varcall";
                    break;
                }
                if (Regex.IsMatch(line, regex_expressions["math"]))
                {
                    confirm = "math";
                    break;
                }
                if (Regex.IsMatch(line,regex_expressions["comment"]))
                {
                    confirm = "comment";
                    break;
                }
                if(line.Trim().Length == 0)
                {
                    confirm = "empty";
                    break;
                }

                if (confirm == null)
                {
                    confirm = nonRegexSyntax + "";
                }
            }
            return confirm;
        }
        /// <summary>
        /// Splits all the captures from the Expression into managable functions
        /// </summary>
        /// <param name="line"></param>
        /// <param name="key"></param>
        /// <returns>Split lines</returns>
        private string[] Regex_SplitLineByType(string line, string key)
        {
            string[] collatedSplits; 
            if(key != "empty")
            {
                GroupCollection collection = Regex.Match(line, regex_expressions[key]).Groups;
                    List<string> temp = new List<string>();

                for(int i = 1; i < collection.Count; i++)
                {
                    temp.Add(collection[i].ToString());
                }

                collatedSplits = temp.ToArray();
            }
            else
            {
                collatedSplits = null;
            }
            return collatedSplits;
        }
        /// <summary>
        /// This replaces the variable names with the values themselves to interpret properply with the RunScript command.
        /// </summary>
        /// <param name="line">A single line of any expression, with or without variables in them.</param>
        private void Regex_SetValuesByVariable(ref string line)
        {
            string returned = "";
            string regex = regex_expressions["varcall"];

            MatchCollection matchCol = Regex.Matches(line, regex);
            if(matchCol.Count == 0)
            {
                //would have thrown error. doesn't need to now.
            }
            else
            { 
                List<string> tempList = new List<string>();


                //Compiles all the matches, to prep replace
                foreach (Match match in matchCol)
                {
                    tempList.Add(match.Groups[2].ToString());
                }

                //replaces all variables with their values
                foreach (string variable in tempList)
                {
                    VariableData data;
                    if (_SavedVariables.TryGetValue(variable, out data))
                    {
                        line = line.Replace("%" + variable, GetVariableData(variable).ToString()); //this isn't nicely handled
                    }
                    else
                    {
                        throw new InvalidVariableException(variable + " does not exist");
                    }
                }
            }
        }
        /// <summary>
        /// The reference changes the math() function
        /// </summary>
        /// <param name="line">references the math function and returns them</param>
        private void Regex_DoMathematicalRender(ref string line)
        {
            int outVal;
            int val1;
            string oper;
            int val2;
            string regex = regex_expressions["math"];

            MatchCollection match = Regex.Matches(line, regex);
            GroupCollection group = match[0].Groups;
            List<string> groupBuilder = new List<string>();
            string[] tempArray;
            string concatArray;

            if (match.Count == 0)
            {
                //would have thrown error. doesn't need to now as it's properly managed
                throw new FailiureToRunScriptException("MatchGroup doesn't exist witihin mathematical render");
            }

            foreach (Capture capture in group)
            {
                groupBuilder.Add(capture.ToString());
            }

            tempArray = groupBuilder.ToArray();
            if (Regex.IsMatch(tempArray[1], regex_expressions["varcall"]))
                Regex_SetValuesByVariable(ref tempArray[1]);

            if (Regex.IsMatch(tempArray[3], regex_expressions["varcall"]))
                Regex_SetValuesByVariable(ref tempArray[3]);

            val1 = int.Parse(tempArray[1]);
            oper = tempArray[2];
            val2 = int.Parse(tempArray[3]);

            DoMath(val1, oper, val2, out outVal);

            line = Convert.ToString(outVal);
        }
        /// <summary>
        /// Processes the lines defined by it's expression
        /// </summary>
        /// <param name="line"></param>
        /// <param name="execute"></param>
        /// <returns></returns>
        public void RunScript(string[] script, params string[] args) //I'd append DrawData and Graphics to this as it's live at time of runScript, constructor not up to date
        {
            #region fields
            string _methodName = "";
            int _MethodArgs = 0;
            bool _methodInstantiation   = false;
            bool _ifInstantiation       = false;
            bool _ifReturnedExact       = false;
            bool _loopInstantiation     = false;
            int _loopAmt                = 0;

            int programCounter  = 0; 
            int methodCounter   = 0;  int methodBeginLine = 0;
            int ifCounter       = 0;  int ifBeginLine     = 0;
            int loopCounter    = 0;   int loopBeginLine   = 0;

            string key;
            #endregion

            foreach(string line in script)
            {
                key = Regex_ConfirmSyntax(line);
                string[] lineSplit = null;
                if(key != "^" || key != "empty")
                {
                    lineSplit = Regex_SplitLineByType(line, key);
                }
                else
                {
                    throw new FailiureToRunScriptException("Line " + programCounter + ": Incorrect Syntax. \n\"" + line + "\"");
                }

                //Checks if ConfirmSyntax finds ill findings
                if (key.StartsWith("^"))
                {
                    //IGNORE FOR NOW
                    //throw new UnknownCallException("");
                }
                if(key == "comment")
                {
                    Console.WriteLine(line);
                }
               
                switch(key)
                {
                    #region variable processing
                    case "varinit": 
                        if(!_methodInstantiation || !_ifInstantiation || !_loopInstantiation)
                        {
                            //Check if math is in use
                            if(Regex.IsMatch(lineSplit[2],regex_expressions["math"]))
                            {
                                Regex_DoMathematicalRender(ref lineSplit[2]);
                            }
                            
                            if (!SetVariable(lineSplit[1], lineSplit[2]))
                            {
                                string str = "Line: " + programCounter + ": " + lineSplit[2] + "is not valid value";
                                throw new InvalidVariableException(str);
                            }
                        }
                        break;
                    #endregion

                    #region method building
                    case "func":
                        if(!_methodInstantiation)
                        {
                            _methodName = lineSplit[1];
                            _MethodArgs = lineSplit[2].Split(',').Length;
                            _methodInstantiation = true;

                            if(_SavedMethods.ContainsKey(_methodName))
                            {
                                throw new MethodExistsInDictionaryException("Method: " + _methodName + " already exists in interpreter. Choose a different name");
                            }

                            methodBeginLine = programCounter;
                        }
                        break;

                    case "funcend":
                        List<string> temp = new List<string>();
                        for(int fc = methodBeginLine + 1; fc <= methodCounter - 1; fc++ )
                        {
                            temp.Add(script[fc]);
                        }

                        MethodData mData = new MethodData()
                        {
                            methodName = _methodName,
                            methodArgs = _MethodArgs,
                            methodBuild = temp.ToArray()
                        };

                        _SavedMethods.Add(_methodName, mData);

                        //Clean Up
                        methodBeginLine = 0;
                        methodCounter = 0;
                        _methodName = "";
                        _MethodArgs = 0;
                        
                        _methodInstantiation = false;

                        break;
                    #endregion

                    #region method calling
                    case "funccall":
                        if (!_methodInstantiation && !_ifInstantiation && !_loopInstantiation)
                        {
                            MethodData innerScript;
                            if (_SavedMethods.TryGetValue(lineSplit[0], out innerScript))
                            {
                                int argumentsCount = innerScript.methodArgs;
                                List<string> argsCounted = new List<string>();
                                string[] innerArgs;
                                string innerArgsName = "argsv";
                                int innerProgramCounter = 0;

                                Regex_SetValuesByVariable(ref lineSplit[1]);
                                innerArgs = lineSplit[1].Split(',');

                                foreach (string val in innerArgs)
                                {
                                    SetVariable(innerArgsName + "" + innerProgramCounter, val);
                                    innerProgramCounter++;
                                }
                                RunScript(innerScript.methodBuild);
                                //CALL FUNCTION WITHOUT THE FUNCTION INITIATION SCOPE
                            }
                            else
                            {
                                throw new MethodNotInstantiatedException();
                            }
                        }
                        break;
                    #endregion

                    #region if processing
                    case "if":
                        if(!_methodInstantiation && !_loopInstantiation)
                        {
                            Regex_SetValuesByVariable(ref lineSplit[1]);
                            Regex_SetValuesByVariable(ref lineSplit[3]);

                            if (DoCondition(int.Parse(lineSplit[1]), lineSplit[2], int.Parse(lineSplit[3])))
                                _ifReturnedExact = true;

                            if (!_ifInstantiation)
                            {
                                _ifInstantiation = true;
                                ifBeginLine = programCounter;
                                Regex_SetValuesByVariable(ref lineSplit[1]);
                                Regex_SetValuesByVariable(ref lineSplit[3]);
                            }
                        }
                        break;

                    case "ifend":
                        if(!_methodInstantiation && !_loopInstantiation)
                        {
                            if(_ifInstantiation)
                            {
                                _ifInstantiation = false;
                                List<string> temp3 = new List<string>();
                                string[] temp3Array;
                                for (int fc = ifBeginLine + 1; fc <= ifCounter + 1; fc++)
                                {
                                    temp3.Add(script[fc]);
                                }

                                temp3Array = temp3.ToArray();
                                if(_ifReturnedExact)
                                    RunScript(temp3Array);

                            }
                        }

                        _ifReturnedExact = false;
                        ifBeginLine = 0;
                        ifCounter = 0;
                        break;
                    #endregion

                    #region loop processing
                    case "loopcall":
                        if (!_methodInstantiation && !_ifInstantiation && !_loopInstantiation)
                        {
                            Regex_SetValuesByVariable(ref lineSplit[2]);

                            _loopAmt = int.Parse(lineSplit[2]);

                            if (!_loopInstantiation)
                            {
                                _loopInstantiation = true;
                                loopBeginLine = programCounter;
                                Regex_SetValuesByVariable(ref lineSplit[2]);
                            }
                        }
                        break;

                    case "loopend":
                        if (!_methodInstantiation && !_ifInstantiation)
                        {
                            if (_loopInstantiation)
                            {
                                _loopInstantiation = false;
                                List<string> temp3 = new List<string>();
                                string[] temp3Array;
                                for (int fc = loopBeginLine + 1; fc <= loopCounter + 1; fc++)
                                {
                                    temp3.Add(script[fc]);
                                }

                                temp3Array = temp3.ToArray();

                                for(int c = 0; c < _loopAmt; c++)
                                    RunScript(temp3Array);
                            }
                        }

                        loopBeginLine = 0;
                        loopCounter = 0;
                        break;

                    #endregion

                    #region shape or pen calling
                    case "shapecall":
                        if(!_methodInstantiation && !_ifInstantiation && !_loopInstantiation)
                        {
                            Regex_SetValuesByVariable(ref lineSplit[1]);

                            string[] argsUnparsed = lineSplit[1].Split(',');
                            int[] parsedArgs = null;
                            List<int> temp2 = new List<int>();
                        
                            foreach (string arg in argsUnparsed)
                            {
                                temp2.Add(int.Parse(arg));
                            }

                            parsedArgs = temp2.ToArray();
                            if (lineSplit[0] == "rectangle")
                            {
                                factory.GetShape("RECTANGLE", drawData, parsedArgs).draw(g);
                            }
                            if(lineSplit[0] == "circle")
                            {
                                factory.GetShape("CIRCLE", drawData, parsedArgs).draw(g);
                            }
                            if(lineSplit[0] == "line")
                            {
                                factory.GetShape("LINE", drawData, parsedArgs).draw(g);
                            }
                            if(lineSplit[0] == "movepen")
                            {
                                MovePen(parsedArgs[0], parsedArgs[1]);
                            }
                            if(lineSplit[0] == "randomcolor")
                            {
                                int r, g, b, t;

                                Random number = new Random();
                                r = number.Next(0, 255);
                                g = number.Next(0, 255);
                                b = number.Next(0, 255);
                                t = number.Next(0, 255);
                                drawData = new DrawData();
                            }
                        }
                        break;
                    #endregion

                    #region inner returnable functions on their own, throw errors
                    case "math":
                        throw new MathNotReturnedToVariableException("Line " + programCounter + ": function needs to be returned to variable.");

                    case "varcall":
                        throw new UnknownCallException("Line " + programCounter + ":" + "Variable needs to be called within fields to return value");
                    #endregion
                }

                //END OF RUN
                if (_methodInstantiation)
                    methodCounter++;

                if (_ifInstantiation)
                    ifCounter++;

                if (_loopInstantiation)
                    loopCounter++;

                programCounter++;
            }
        }
        /// <summary>
        /// Moves Pen inline with DrawData. Can out this to an outside class if nessecary.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="data"></param>
        public void MovePen(int x, int y)
        {
            drawData.SetOriginPosition(x, y);
        }
    }

    struct MethodData
    {
        public string methodName;
        public int methodArgs;
        public string[] methodBuild;
    }
    struct VariableData
    {
        public string varName;
        public string varData;
    }

}
