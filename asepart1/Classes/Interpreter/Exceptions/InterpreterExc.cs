﻿using System;

namespace asepart1
{
    public class FailiureToRunScriptException : Exception
    {
        public FailiureToRunScriptException()
        {

        }

        public FailiureToRunScriptException(string message) : base(message)
        {

        }

        public FailiureToRunScriptException(string message, Exception inner) : base(message, inner)
        {

        }
    }
    public class UnknownCallException : Exception
    {
        public UnknownCallException() : base()
        {

        }

        public UnknownCallException(string message) : base(message)
        {

        }

        public UnknownCallException(string message, Exception inner) : base(message, inner)
        {

        }
    }
    public class InvalidVariableException : Exception
    {
        public InvalidVariableException() : base()
        {

        }

        public InvalidVariableException(string message) : base(message)
        {

        }

        public InvalidVariableException(string message, Exception inner) : base(message)
        {

        }
    }
    public class ShapeInvalidException : Exception
    {
        public ShapeInvalidException()
        {

        }

        public ShapeInvalidException(string message) : base(message)
        {

        }

        public ShapeInvalidException(string message, Exception inner) : base(message, inner)
        {

        }
    }
    public class InnerMethodInvalidException : Exception
    { 
        public InnerMethodInvalidException() : base()
        {

        }

        public InnerMethodInvalidException(string message) : base(message)
        {

        }

        public InnerMethodInvalidException(string message, Exception inner) : base(message, inner)
        {

        }
    }
    public class MethodNotInstantiatedException : Exception
    {
        public MethodNotInstantiatedException() : base()
        {
            
        }

        public MethodNotInstantiatedException(string message) : base(message)
        {

        }
        
        public MethodNotInstantiatedException(string message, Exception inner) : base(message, inner)
        {

        }
    }
    public class MethodExistsInDictionaryException : Exception
    {
        public MethodExistsInDictionaryException() : base()
        {

        }

        public MethodExistsInDictionaryException(string message) : base(message)
        {

        }

        public MethodExistsInDictionaryException(string message, Exception inner) : base(message)
        {

        }
    }
    public class MathNotReturnedToVariableException : Exception
    {
        public MathNotReturnedToVariableException() : base()
        {

        }

        public MathNotReturnedToVariableException(string message) : base(message)
        {

        }

        public MathNotReturnedToVariableException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
