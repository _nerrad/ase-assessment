﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Reflection;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace asepart1
{
    class Commander
    {
        public static string[] ParseCommand(string command)
        {
            string[] commandDelimited = new string[] { "0" }; //Will return this value if false. Otherwise array will be re-written
            Regex regExParenthesis;
            Match match;
            
            //extracts Regular Expression, brackets in parenthesis " ^(?<command>.*?)(?<arguments>\((.*?)\)) "
            regExParenthesis = new Regex(@"^(?<command>.*?)(?<arguments>\((.*?)\))", 
                                        RegexOptions.IgnoreCase | RegexOptions.Singleline); //RegexOptions FlagsAttribute, uses bitwise logic
            match = regExParenthesis.Match(command);
            
            if (match.Success)
            {
                List<string> collectionAdd = new List<string>();

                for (int i = 2; i <= match.Groups.Count; i++)
                {
                    Group mGroup = match.Groups[i];
                    CaptureCollection captures = mGroup.Captures;
                    for (int x = 0; x < captures.Count; x++)
                    {
                        Capture capture = captures[x];
                        collectionAdd.Add(capture.Value);
                    }
                }
                commandDelimited = collectionAdd.ToArray();

                //Tidy arguments collection
                if (commandDelimited.Length == 2)
                {
                    commandDelimited[1] = commandDelimited[1].Replace((char)'(', ' ')
                                                            .Replace((char)')', ' ')
                                                            .Trim();
                }
            }
            return commandDelimited;
        }

    }
}
