﻿using System.Drawing;

namespace asepart1
{
    class Rectangle : Shape
    {
        private int width, height;
        public Rectangle() : base()
        {
            width = 100;
            height = 200;
        }

        public Rectangle(DrawData d, int width, int height) : base(d)
        {
            this.width = width;
            this.height = height;
        }

        public override void set(DrawData d, params int[] list)
        {
            drawData = d;
            this.width = list[0];
            this.height = list[1];
        }

        public override void draw(Graphics g)
        {
            g.DrawRectangle(drawData.GetPen(), drawData.GetOriginPosition()[0],
                            drawData.GetOriginPosition()[1],
                            width,
                            height);

            g.FillRectangle(drawData.GetSolidBrush(), drawData.GetOriginPosition()[0],
                            drawData.GetOriginPosition()[1],
                            width,
                            height);

        }

        public Point[] GetCornerPosition()
        {
            int[] origPt = drawData.GetOriginPosition();

            Point[] points = new Point[]
            {
                new Point(origPt[0],origPt[1]),                     //Top Left
                new Point(origPt[0] + width,origPt[1]),             //Top Right
                new Point(origPt[0], origPt[1] + height),           //Bottom Left
                new Point(origPt[0] + width, origPt[1] + height)    //Bottom Right
            };

            return points;
        }
    }
}
