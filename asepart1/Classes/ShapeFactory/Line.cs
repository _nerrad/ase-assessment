﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace asepart1
{
    class Line : Shape
    {
        private Point endPoint;
        

        public Line() : base()
        {
            endPoint = new Point(0, 0);
        }

        public Line(DrawData d, int endX, int endY) : base(d)
        {
            drawData = d;
            endPoint = new Point(endX, endY);
        }

        public override void set(DrawData d, params int[] list)
        {
            drawData = d;
            endPoint = new Point(list[0], list[1]);
        }

        public override void draw(Graphics g)
        {
            g.DrawLine(drawData.GetPen(),
                drawData.GetOriginPosition()[0],
                drawData.GetOriginPosition()[1],
                endPoint.X,
                endPoint.Y);
        }
    }
}
