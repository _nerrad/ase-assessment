﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace asepart1
{
    abstract class Shape : IShapes
    {
        protected DrawData drawData;
        /// <summary>
        /// Constructor for a standard shape. Sets color as black, pen location as default
        /// </summary>
        public Shape()
        {
            drawData = new DrawData(Color.Black, Color.Transparent, 1, new Point(100, 100));
        }

        /// <summary>
        /// Constructor for a standard shape
        /// </summary>
        /// <param name="color">Color: sets the color as appropriate</param>
        /// <param name="x">int: sets the x axis</param>
        /// <param name="y">int: sets the y axis</param>
        public Shape(DrawData d)
        {
            drawData = d;
        }

        /// <summary>
        /// Abstract, defined custom by shape
        /// </summary>
        /// <param name="g">Graphics Interface will be encapsulated</param>
        public abstract void draw(Graphics g);
    
        /// <summary>
        /// Changes properties. Method can be changed on derived class
        /// </summary>
        /// <param name="color">Color: changes the color</param>
        /// <param name="list">Params: allows you to enter an extensive amount of arguments as an Integer</param>
        public virtual void set(DrawData d, params int[] list)
        {
            drawData = d;
        }
    }
}
