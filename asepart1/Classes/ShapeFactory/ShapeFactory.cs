﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asepart1
{
    class ShapeFactory
    {
        public Shape GetShape(string shapeType, DrawData data, params int[] extras)
        {
            shapeType = shapeType.ToUpper().Trim();
            Shape shape;
            switch(shapeType)
            {
                case "CIRCLE":
                    shape = new Circle(data, extras[0]);
                    return shape;

                case "RECTANGLE":
                    shape = new Rectangle(data, extras[0], extras[1]);
                    return shape;

                case "LINE":
                    shape = new Line(data, extras[0], extras[1]);
                    return shape;

                case "TRIANGLE":
                    shape = new Triangle(); //needs finalising
                    return shape;

                default:
                    System.ArgumentException argEx = new System.ArgumentException("Factory error: " + shapeType + " does not exist");
                    throw argEx;
            }
        }
    }
}
