﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace asepart1
{
    class Triangle : Shape
    {
        /// <summary>
        /// Currently only allows Equilateral, Isoceles & Right Angle Triangles currently
        /// </summary>
        public Triangle() : base()
        {

        }

        public Triangle(DrawData d) : base(d)
        {
 
        }

        public override void set(DrawData d, params int[] list)
        {
            drawData = d;
            //follow Constructor 2
        }

        public override void draw(Graphics g)
        {

        }
    }
}
