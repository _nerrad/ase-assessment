﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace asepart1
{
    class Circle : Shape
    {
        private int radius;
        public Circle() : base()
        {
            radius = 100;
        }

        public Circle(DrawData d, int radius) : base(d)
        {
            this.radius = radius;
        }

        public override void set(DrawData d, params int[] list)
        {
            drawData = d;
            radius = list[0];
        }

        public override void draw(Graphics g)
        {
            g.DrawEllipse(drawData.GetPen(), 
                            drawData.GetOriginPosition()[0], 
                            drawData.GetOriginPosition()[1], 
                            radius * 2, radius * 2);

            g.FillEllipse(drawData.GetSolidBrush(),
                            drawData.GetOriginPosition()[0],
                            drawData.GetOriginPosition()[1],
                            radius * 2, radius * 2);
        }
    }
}
