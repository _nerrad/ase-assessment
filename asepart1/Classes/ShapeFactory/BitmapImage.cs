﻿using System.Drawing;

namespace asepart1
{
    class BitmapImage : Shape
    {
        Image image;
        public BitmapImage() : base()
        {
            drawData.SetOriginPosition(0, 0);
            image = Image.FromFile("smallanimal.png"); //Needs adjusting accordingly
        }

        public BitmapImage(DrawData d, Image image) : base(d)
        {
            this.image = image;
        }

        public override void set(DrawData d, params int[] list)
        {
            drawData = d;
        }

        public override void draw(Graphics g)
        {
            g.DrawImage(image, 
                drawData.GetOriginPosition()[0], 
                drawData.GetOriginPosition()[1]);
        }

    }
}
