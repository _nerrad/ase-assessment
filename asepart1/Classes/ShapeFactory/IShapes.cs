﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace asepart1
{
    interface IShapes
    {
        void set(DrawData d, params int[] list);
        void draw(Graphics g);
    }
}
