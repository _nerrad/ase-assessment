﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Reflection;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace asepart1
{
    /// <summary>
    /// 
    /// </summary>
    public class DrawData
    {
        //Fields
        private Pen pen;
        private SolidBrush brush;
        private Color lineColor;
        private Color fillColor;
        private float weight;
        private Point origin;

        //Constructors
        public DrawData()
        {
            lineColor = Color.Black;
            fillColor = Color.Transparent;

            weight = 1;

            origin = new Point(0, 0);

            BuildPen();
            BuildBrush();
        }
        public DrawData(Color line, Color fill, float weight, Point penOrigin)
        {
            lineColor = line;
            fillColor = fill;

            this.weight = weight;

            origin = penOrigin;

            BuildPen();
            BuildBrush();
        }

        //Methods
        private void BuildPen()
        {
            pen = new Pen(lineColor, weight);
        }
        private void BuildBrush()
        {
            brush = new SolidBrush(fillColor);
        }

        //Accessor
        public Pen GetPen()
        {
            return pen;
        }
        public SolidBrush GetSolidBrush()
        {
            return brush;
        }
        public int[] GetOriginPosition()
        {
            return new int[] { origin.X, origin.Y };
        }
        public void SetOriginPosition(int x, int y)
        {
            origin.X = x;
            origin.Y = y;
        }
            
    }
}
