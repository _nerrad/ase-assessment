﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using asepart1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace asepart1.Tests
{
    [TestClass()]
    public class InterpreterTests
    {
        [TestMethod()]
        public void regexConfirmSyntax_testSyntax()
        {
            DrawData d = new DrawData();
            
            Graphics g = Graphics.FromImage(new Bitmap(20,20));
            Interpreter interpreter = new Interpreter(d,g);
            string[] str = new string[] {"func method(test,test)",
            "funcend",
            "if x == y",
            "if x != y",
            "ifend"};
            
            foreach(string cmd in str)
            {
                if(!interpreter.regexConfirmSyntax(cmd))
                {
                    Assert.Fail("Failed on:" + cmd);
                }
            }
        }
    }
}